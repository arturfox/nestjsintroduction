import { WebSocketGateway, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit } from '@nestjs/websockets';

@WebSocketGateway()
export class NotificatorGateWay implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer() server;

    public notifiUserByUserId(userId: string) {

        this.server.emit(`refresh/${userId}`);
    }

    afterInit() {

    }

    async handleConnection() {

    }

    async handleDisconnect() {

    }



}