import { Module } from '@nestjs/common';
import { NotificatorGateWay } from './notificator.gateway';

@Module({
    providers: [NotificatorGateWay],
    controllers: [/*NotificatorGateWay*/],
    exports: [NotificatorGateWay]
})
export class NotificatorModule { }
