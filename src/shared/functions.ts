import { ResultModel } from "./result-model";
import { ResultHttpModel } from "./result-http-model";

export function getHttpResultModelByResultModel(resultModel: ResultModel<any>) : ResultHttpModel{

    var result = new ResultHttpModel();

    result.data = resultModel.data;
    result.message=resultModel.message;
    result.isSuceed = resultModel.isSuceed;
    result.statusCode =  (result.isSuceed)? 200 : 400;
    
    return result;
}
