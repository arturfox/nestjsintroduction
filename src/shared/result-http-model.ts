import { ResultModel } from "./result-model";

export class ResultHttpModel extends ResultModel {
    
    public statusCode: number;
}
