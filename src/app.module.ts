import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TasksModule } from './tasks/tasks.module';
import { Task } from './tasks/task.entity';
import { SharedModule } from './shared/shared.module';
import { User } from './users/user.entity';
import { RefreshToken } from './auth/refresh-token.entity';
import { NotificatorModule } from './notificator/notificator.module';

@Module({
  imports: [AuthModule, 
    UsersModule,
    TasksModule,
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'localhost',
      port: 27017,
      database: 'taskdb',
      entities: [Task,User,RefreshToken],
      synchronize: true
    }),
    SharedModule,
    NotificatorModule
  ],
  exports: [SharedModule]
})
export class AppModule { }
