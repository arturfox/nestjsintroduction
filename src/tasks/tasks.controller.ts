import { Controller, Get, UseGuards, Request, Post, Body } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { AuthGuard } from '@nestjs/passport';
import { getHttpResultModelByResultModel } from '../shared/functions';
import { CreateTaskModel } from './view-models/Models/create-task.model';
import { UpdateTaskStatusModel } from './view-models/models/update-task-status.model';
import { EditTaskModel } from './view-models/models/edit-task.model';
import { DeleteTaskModel } from './view-models/models/delete-task.model';

@Controller('tasks')
export class TasksController {
    constructor(private readonly tasksService: TasksService) {
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('addTask')
    async addTask(@Body() taskModel: CreateTaskModel, @Request() request) {

        const addTaskResult = await this.tasksService.addTask(taskModel, request.user.userId);
        const result = getHttpResultModelByResultModel(addTaskResult);

        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('getTasks')
    async getTasksByUserId(@Request() request) {

        const getTasksResult = await this.tasksService.getTasksByUserId(request.user.userId);
        const result = getHttpResultModelByResultModel(getTasksResult);

        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('updateTaskStatus')
    async updateTaskStatus(@Body() statusModel: UpdateTaskStatusModel, @Request() request) {

        const updateStatusResult = await this.tasksService.updateTaskStatus(statusModel, request.user.userId);
        const result = getHttpResultModelByResultModel(updateStatusResult);

        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('editTask')
    async editTask(@Body() editModel: EditTaskModel, @Request() request) {

        const editResult = await this.tasksService.editTask(editModel, request.user.userId);
        const result = getHttpResultModelByResultModel(editResult);

        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('deleteTask')
    async deleteTask(@Body() deleteModel: DeleteTaskModel, @Request() request) {

        const deleteResult = await this.tasksService.deleteTask(deleteModel, request.user.userId);
        const result = getHttpResultModelByResultModel(deleteResult);

        return result;
    }
}
