import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';
import { TaskStatus } from './taskstatus.enum';
import { CreateTaskResponseViewModel } from './view-models/create-task-response.viewmodel';
import { ResultModel } from '../shared/result-model';
import { CreateTaskModel } from './view-models/Models/create-task.model';
import { TaskCoordinates } from './task-coordinates';
import { ObjectID } from 'typeorm';
import { TaskCoordinatesView } from './view-models/task-coordinates-view';
import { GetTasksByUserIdViewModel } from './view-models/get-tasks-by-user-id.viewmodel';
import { GetTasksByUserIdViewItem } from './view-models/get-tasks-by-user-id.viewitem';
import { UpdateTaskStatusModel } from './view-models/models/update-task-status.model';
import { UpdateTaskStatusResponseViewModel } from './view-models/update-task-status-response.viewmodel';
import { EditTaskModel } from './view-models/models/edit-task.model';
import { EditTaskResponseViewModel } from './view-models/edit-task-response.viewmodel';
import { DeleteTaskModel } from './view-models/models/delete-task.model';
import { PhotoView } from './view-models/photo.view';
import { Photo } from './photo';
import { NotificatorGateWay } from '../notificator/notificator.gateway';

@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(Task)
        private readonly taskRepository: TaskRepository,
        private readonly notificationService: NotificatorGateWay) { }

    async addTask(createTaskModel: CreateTaskModel, userId: ObjectID): Promise<ResultModel<CreateTaskResponseViewModel>> {

        let result = new ResultModel<CreateTaskResponseViewModel>();

        let task = new Task();
        task.title = createTaskModel.title;
        task.description = createTaskModel.description;
        task.status = (createTaskModel.isStarted) ? TaskStatus.Started
            : TaskStatus.Created;

        if (createTaskModel.coordinates != null || createTaskModel.coordinates != undefined) {
            task.coordinates = new TaskCoordinates();
            task.coordinates.lat = createTaskModel.coordinates.lat;
            task.coordinates.lng = createTaskModel.coordinates.lng;
        }

        if (createTaskModel.photo != null || createTaskModel.photo != undefined) {
            task.photo = new Photo();
            task.photo.data = createTaskModel.photo.data;
        }
        task.userId = userId.toString();

        let insertData = await this.taskRepository.insert(task);

        if (insertData == null || insertData.generatedMaps == null
            || insertData.generatedMaps.length == 0) {

            result.isSuceed = false;
            result.message = "something went wrong while saving task"

            return result;
        }

        let taskId = insertData.generatedMaps[0].id;

        result.isSuceed = true;
        result.data = new CreateTaskResponseViewModel();
        result.data.title = createTaskModel.title;
        result.data.status = task.status;
        result.data.description = createTaskModel.description;
        result.data.id = taskId.toString();

        if (createTaskModel.coordinates != null
            || createTaskModel.coordinates != undefined) {
            result.data.coordinates = new TaskCoordinatesView();
            result.data.coordinates.lat = createTaskModel.coordinates.lat;
            result.data.coordinates.lng = createTaskModel.coordinates.lng;
        }

        if (createTaskModel.photo != null
            || createTaskModel.photo != undefined) {
            
                result.data.photo= new PhotoView();
                result.data.photo.data = task.photo.data;
        }
        
        this.notificationService.notifiUserByUserId(userId.toString());

        return result;
    }

    async getTasksByUserId(userId: ObjectID): Promise<ResultModel<GetTasksByUserIdViewModel>> {

        let result = new ResultModel<GetTasksByUserIdViewModel>();
        let tasksModel = new GetTasksByUserIdViewModel();

        let tasks = (await this.taskRepository.find())
            .filter(task => task.userId == userId.toString())
            .sort((first,second) => (first.status.valueOf() < second.status.valueOf()) ? -1 : 1 );

        tasks.forEach((task, index) => {

            let model = new GetTasksByUserIdViewItem();
            if (task.coordinates != null) {

                model.coordinates = new TaskCoordinatesView();
                model.coordinates.lat = task.coordinates.lat;
                model.coordinates.lng = task.coordinates.lng;
            }

            if(task.photo != null){

                model.photo= new PhotoView();
                model.photo.data = task.photo.data;
            }

            model.description = task.description;
            model.id = task.id.toString();
            model.status = task.status;
            model.title = task.title;
            tasksModel.tasks.push(model);
        });

        tasksModel.userId = userId.toString();

        result.isSuceed = true;
        result.data = tasksModel;

        return result;
    }

    //TODO: find a way to detect update entity result
    async updateTaskStatus(statusModel: UpdateTaskStatusModel, userId: ObjectID): Promise<ResultModel<UpdateTaskStatusResponseViewModel>> {

        let result = new ResultModel<UpdateTaskStatusResponseViewModel>();
        let task = await this.taskRepository.findOne(statusModel.taskId);
        if (task == null) {
            result.isSuceed = false;
            result.message = "task not found";

            return result;
        }

        if (task.userId != userId.toString()) {
            result.isSuceed = false;
            result.message = "Invalid update state: task's owner is different";

            return result;
        }

        task.status = statusModel.status;

        await this.taskRepository.update(task.id.toString(), { status: statusModel.status });

        result.isSuceed = true;
        result.message="task's status successfully updated.";

        result.data = new UpdateTaskStatusResponseViewModel();
        result.data.id = task.id.toString();
        result.data.status = task.status;
        result.data.title = task.title;
        result.data.description = task.description;
        if (task.coordinates != null) {

            result.data.coordinates = new TaskCoordinatesView();
            result.data.coordinates.lat = task.coordinates.lat;
            result.data.coordinates.lng = task.coordinates.lng;
        }

        if(task.photo != null){

            result.data.photo = new PhotoView();
            result.data.photo.data = task.photo.data; 
        }

        return result;
    }

    //TODO: find a way to detect update entity result
    async  editTask(editModel: EditTaskModel, userId: ObjectID): Promise<ResultModel<EditTaskResponseViewModel>> {

        let result = new ResultModel<EditTaskResponseViewModel>();
        let task = await this.taskRepository.findOne(editModel.id);
        if (task == null) {
            result.isSuceed = false;
            result.message = "task not found";

            return result;
        }


        if (task.userId != userId.toString()) {

            result.isSuceed = false;
            result.message = "Invalid update state: task's owner is different";

            return result;
        }

        task.description = editModel.description;

        await this.taskRepository.update(task.id.toString(), { description: task.description });

        result.message = "task successfully updated";
        result.isSuceed = true;
        result.data = new EditTaskResponseViewModel();
        result.data.id = task.id.toString();
        result.data.status = task.status;
        result.data.title = task.title;
        result.data.description = task.description;
        if (task.coordinates != null) {

            result.data.coordinates = new TaskCoordinatesView();
            result.data.coordinates.lat = task.coordinates.lat;
            result.data.coordinates.lng = task.coordinates.lng;
        }

        if(task.photo != null){

            result.data.photo = new PhotoView();
            result.data.photo.data = task.photo.data; 
        }

        return result;
    }

    async deleteTask(deleteTask: DeleteTaskModel, userId: ObjectID): Promise<ResultModel> {

        let result = new ResultModel();
        let task = await this.taskRepository.findOne(deleteTask.id);
        if (task == null) {
            result.isSuceed = false;
            result.message = "task not found";

            return result;
        }

        if (task.userId != userId.toString()) {

            result.isSuceed = false;
            result.message = "Invalid state: task's owner is different";

            return result;
        }

        await this.taskRepository.remove(task);

        result.isSuceed = true;
        result.message = "task successfully removed";

        return result;
    }
}
