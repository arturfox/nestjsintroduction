import { Entity, Column, ObjectID, ObjectIdColumn } from 'typeorm';
import { TaskStatus } from './taskstatus.enum';
import { TaskCoordinates } from './task-coordinates';
import { Photo } from './photo';

@Entity()
export class Task {
  
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  status: TaskStatus;

  @Column()
  userId: string;

  @Column()
  coordinates: TaskCoordinates;

  @Column()
  photo: Photo;
}