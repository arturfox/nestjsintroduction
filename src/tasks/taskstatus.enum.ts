export enum TaskStatus {
    Created = 0,
    Started = 1,
    Finished = 2
}