import { TaskCoordinatesModel } from "./task-coordinates.model";
import { IsString, IsBooleanString } from "class-validator";
import { PhotoModel } from "./photo.model";

export class CreateTaskModel {

    @IsString()
    public title:string;

    @IsString()
    public description:string;

    @IsBooleanString()
    public isStarted:  boolean;

    public coordinates: TaskCoordinatesModel;

    public photo: PhotoModel;
}
