import { IsString } from "class-validator";

export class EditTaskModel {

    @IsString()
    public description: string;
    @IsString()
    public id: string;
}
