import { TaskStatus } from "../../taskstatus.enum";
import { IsString, IsEnum } from "class-validator";

export class UpdateTaskStatusModel {

    @IsString()
    public taskId : string;

    @IsEnum(TaskStatus)
    public status: TaskStatus;
}
