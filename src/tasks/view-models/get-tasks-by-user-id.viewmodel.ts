import { GetTasksByUserIdViewItem } from "./get-tasks-by-user-id.viewitem";

export class GetTasksByUserIdViewModel {

    public userId: string;
    public tasks: Array<GetTasksByUserIdViewItem>;

    constructor() {

        this.tasks = new Array<GetTasksByUserIdViewItem>();
    }
}
