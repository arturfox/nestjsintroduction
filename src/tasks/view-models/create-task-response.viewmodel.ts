import { TaskCoordinatesView } from "./task-coordinates-view";
import { TaskStatus } from "../taskstatus.enum";
import { PhotoView } from "./photo.view";

export class CreateTaskResponseViewModel {

    public title:string;
    public description:string;
    public status:  TaskStatus;
    public coordinates: TaskCoordinatesView;
    public photo: PhotoView;

    public id: string;
}
