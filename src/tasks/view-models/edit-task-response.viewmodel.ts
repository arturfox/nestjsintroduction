import { TaskStatus } from "../taskstatus.enum";
import { TaskCoordinatesView } from "./task-coordinates-view";
import { PhotoView } from "./photo.view";

export class EditTaskResponseViewModel {

    public title:string;
    public description:string;
    public status:  TaskStatus;
    public coordinates: TaskCoordinatesView;
    public photo: PhotoView;

    public id: string;
}
