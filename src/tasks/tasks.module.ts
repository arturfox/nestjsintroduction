import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskRepository } from './task.repository';
import { TasksController } from './tasks.controller';
import { SharedModule } from '../shared/shared.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../auth/constants';
import { NotificatorModule } from '../notificator/notificator.module';

@Module({
  imports: [SharedModule,
    TypeOrmModule.forFeature([TaskRepository]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn:jwtConstants.expiresIn },
    }),
    NotificatorModule
  ],
  exports: [TypeOrmModule],
  providers: [TasksService],
  controllers: [TasksController]
})
export class TasksModule { }
