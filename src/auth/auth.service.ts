import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { ResultModel } from '../shared/result-model';
import { User } from '../users/user.entity';
import { AuthenticationType } from '../users/authentification-type.enum';
import { ResponseLoginModel } from './response-login.model';
import * as crypto from 'crypto';
import { LoginModel } from './view-models/models/login.model';
import { RegisterModel } from './view-models/models/register.model';
import { jwtConstants, refreshConstants } from './constants';
import * as UUID from 'uuid';
import { RefreshTokenRepository } from './refresh-token.repository';
import { RefreshToken } from './refresh-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { RefreshTokenModel } from './view-models/models/refresh-token.model';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    @InjectRepository(RefreshToken)
    private readonly refreshTokenRepository: RefreshTokenRepository) { }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.getByUserName(username);
    if (user && user.password === crypto.createHmac('sha256', pass).digest('hex')) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  //TODO: add AcessToken blacklist
  public async login(loginModel: LoginModel): Promise<ResultModel<ResponseLoginModel>> {

    let result = new ResultModel<ResponseLoginModel>();

    if (loginModel.authorizationType == AuthenticationType.Default) {
      result = await this.loginDefault(loginModel);
      return result;
    }

    if (loginModel.authorizationType == AuthenticationType.FaceBook
      || loginModel.authorizationType == AuthenticationType.GooglePlus) {
      result = await this.loginAuth(loginModel);
      return result;
    }

    result.isSuceed = false;
    result.message = "Invalid AuthenticationType";

    return result;
  }

  public async GetUpdatedTokensByRefreshToken(refreshModel: RefreshTokenModel): Promise<ResultModel<ResponseLoginModel>> {

    let result = new ResultModel<ResponseLoginModel>();

    let token = await this.refreshTokenRepository.getByTokenBody(refreshModel.refreshToken);

    if (token == null || !token.isValid) {

      result.isSuceed = false;
      result.message = "refresh token is invalid";

      return result;
    }

    let criationdate = +token.creationDate;
    let lifetime = +token.lifeTime * refreshConstants.secondsToMillisecondsCoefficient;

    let checkDate =  new Date(criationdate);
    let tokenDate = new Date(criationdate + lifetime);

    if (tokenDate.valueOf() < Date.now()) {

      result.isSuceed = false;
      result.message = "refresh token is invalid";

      return result;
    }

    const existingUser = await this.usersService.getByUserId(token.userId);
    if (existingUser == null) {

      result.isSuceed = false;
      result.message = "User is not found";

      return result;
    }

    await this.refreshTokenRepository.update(token.id.toString(), { isValid: false });

    result.isSuceed = true;
    result.message = "User successfully authorized";
    result.data = await this.GetResponseLoginModelByUser(existingUser, this.jwtService);

    return result;

  }

  public async register(registerModel: RegisterModel): Promise<ResultModel> {

    let result = new ResultModel();

    if (registerModel.password !== registerModel.confirmPassword) {
      result.isSuceed = false;
      result.message = "password and re-password do not match";

      return result;
    }

    const existingUser = await this.usersService.getByUserName(registerModel.username);

    if (existingUser) {
      result.isSuceed = false;
      result.message = "a user with the same name already exists";

      return result;
    }

    var user = new User();
    user.userName = registerModel.username;
    user.password = crypto.createHmac('sha256', registerModel.password).digest('hex');

    user.authentificationType = AuthenticationType.Default;

    const createResult = await this.usersService.AddUser(user);
    result.isSuceed = createResult.isSuceed;

    result.message = (result.isSuceed) ? "account successfully created"
      : "something went wrong while creating account";

    return result;
  }

  private async loginDefault(loginModel: LoginModel): Promise<ResultModel<ResponseLoginModel>> {

    let result = new ResultModel<ResponseLoginModel>();
    const existingUser = await this.usersService.getByUserName(loginModel.username);

    if (existingUser == null || existingUser == undefined
      || existingUser.authentificationType != AuthenticationType.Default) {

      result.isSuceed = false;
      result.message = "User is not found";

      return result;
    }

    const hashPassword = crypto.createHmac('sha256', loginModel.password).digest('hex');
    if (hashPassword != existingUser.password) {

      result.isSuceed = false;
      result.message = "Username or password is incorrect";

      return result;
    }

    result.isSuceed = true;
    result.message = "User successfully authorized";
    result.data = await this.GetResponseLoginModelByUser(existingUser, this.jwtService);

    return result;
  }

  //TODO:add validation on authToken(gmail,facebook)
  private async loginAuth(loginModel: LoginModel): Promise<ResultModel<ResponseLoginModel>> {

    let result = new ResultModel<ResponseLoginModel>();
    const existingUser = await this.usersService.getByUserName(loginModel.username)

    if (existingUser == null || existingUser == undefined
      || existingUser.authentificationType != loginModel.authorizationType) {

      var user = new User();
      user.userName = loginModel.username;

      user.authentificationType = loginModel.authorizationType;

      await this.usersService.AddUser(user);

      const createdUser = await this.usersService.getByUserName(user.userName);

      if (createdUser == null) {

        result.message = "authorization failed - can't register new user";
      }

      result.isSuceed = true;
      result.data = await this.GetResponseLoginModelByUser(createdUser, this.jwtService);
      result.message = "User successfully authorized";
      return result;
    }

    result.isSuceed = true;
    result.data = await this.GetResponseLoginModelByUser(existingUser, this.jwtService);
    result.message = "User successfully authorized";
    return result;
  }

  private async  GetResponseLoginModelByUser(user: User, jwtService: JwtService): Promise<ResponseLoginModel> {

    let responseModel = new ResponseLoginModel();

    responseModel.userId = user.id.toString();
    responseModel.authType = user.authentificationType;
    responseModel.username = user.userName;
    responseModel.accessToken = await jwtService.signAsync({
      username: user.userName,
      authType: user.authentificationType,
      userId: user.id
    },
      {
        expiresIn: jwtConstants.expiresIn
      }
    );
    responseModel.refreshToken = await this.generateRefreshToken(responseModel.userId, responseModel.accessToken);

    return responseModel;
  }

  private async generateRefreshToken(userId: string, accessToken: string): Promise<string> {

    let token = new RefreshToken();
    token.acessTokenPrint = accessToken;
    token.isValid = true;
    token.lifeTime = refreshConstants.expiresIn;
    token.creationDate = Date.now().toString();
    token.userId = userId;
    token.token = UUID.v4();

    await this.refreshTokenRepository.insert(token);
    return token.token;

  }


}
