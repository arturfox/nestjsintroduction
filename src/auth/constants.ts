export const jwtConstants = {
    secret: 'secretKey',
    expiresIn: '3600s'
  };

  export const refreshConstants = {
    expiresIn: '86400',
    secondsToMillisecondsCoefficient: 1000
  };