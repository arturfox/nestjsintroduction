import { MinLength, IsString } from 'class-validator';

export class RegisterModel{

    @IsString()
    public username: string;

    @IsString()
    @MinLength(6)
    public password: string;

    @IsString()
    @MinLength(6)
    public confirmPassword: string;
}