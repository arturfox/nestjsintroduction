import { IsString, IsEnum } from 'class-validator';
import { AuthenticationType } from '../../../users/authentification-type.enum';

export class LoginModel {

    @IsString()
    public username: string;

    @IsEnum(AuthenticationType)
    public authorizationType: AuthenticationType;

    public password: string;

    public accessToken: string;
}