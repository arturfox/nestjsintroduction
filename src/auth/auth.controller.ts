import { Controller, Post, Get, UseGuards, Request, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../auth/auth.service';
import { getHttpResultModelByResultModel } from '../shared/functions';
import { ResultHttpModel } from '../shared/result-http-model';
import { LoginModel } from './view-models/models/login.model';
import { RegisterModel } from './view-models/models/register.model';
import { RefreshTokenModel } from './view-models/models/refresh-token.model';

@Controller('account')
export class AuthController {

    constructor(private readonly authService: AuthService) { }

    //@UseGuards(AuthGuard('local'))
    @Post("login")
    async login(@Body() loginModel: LoginModel) {

        let loginResult = await this.authService.login(loginModel);
        let result = getHttpResultModelByResultModel(loginResult);

        return result;

    }

    @Post("register")
    async Register(@Body() registerModel: RegisterModel) {
 
        let registerResult = await this.authService.register(registerModel);
        let result = getHttpResultModelByResultModel(registerResult);

        return result;

    }

    @UseGuards(AuthGuard('jwt'))
    @Get("logout")
    async logOut(@Request() request) {

        let logOutResult = new ResultHttpModel();
        logOutResult.isSuceed = true;
        logOutResult.statusCode = 200;
        logOutResult.message = "logout successfully processed";

        return logOutResult;
    }

    @Post("token/refresh")
    async RefreshToken(@Body() refreshModel: RefreshTokenModel) {

        let updatedTokensResult = await this.authService.GetUpdatedTokensByRefreshToken(refreshModel);

        let result = getHttpResultModelByResultModel(updatedTokensResult);

        return result;
    }
}