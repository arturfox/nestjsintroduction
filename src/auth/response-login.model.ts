import { AuthenticationType } from "../users/authentification-type.enum";

export class ResponseLoginModel {

    public username: string;
    public accessToken: string;
    public refreshToken: string;
    public userId: string;

    public authType: AuthenticationType;
}