import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserRepository } from './user.repository';
import { ResultModel } from '../shared/result-model';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User)
    private readonly userRepository: UserRepository) {
  }

  public async getByUserName(username: string): Promise<User> {

    return await this.userRepository.getByUserName(username);
  }

  public async getByUserId(userId: string): Promise<User> {

    return await this.userRepository.findOne(userId);
  }

  public async AddUser(user: User): Promise<ResultModel> {

    var result = new ResultModel();

    const insertResult = await this.userRepository.insert(user);

    result.isSuceed = !(insertResult.generatedMaps == null
      || insertResult.generatedMaps == undefined);

    return result;
  }

}
