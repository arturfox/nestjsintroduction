import { Entity, Column, BeforeInsert, ObjectIdColumn, ObjectID } from 'typeorm';
import { AuthenticationType } from './authentification-type.enum';

@Entity()
export class User {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    userName: string;

    @Column()
    authentificationType: AuthenticationType;

    @Column()
    password: string;
}
