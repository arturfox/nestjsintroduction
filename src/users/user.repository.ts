import { Repository, EntityRepository } from "typeorm";
import { User } from "./user.entity";

@EntityRepository(User)
export class UserRepository extends Repository<User>{

    async getByUserName(userName: string): Promise<User>{

        return await this.findOne({
            where:{
                userName: userName
            }
        });
    }
}